<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Reservasi;
use App\Ruangan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pagetitle = "Dashboard";

        $today = Carbon::now();
        $tahun = $today->year;
        $bulan = $today->month;
        $namabulan = ['Januari', 'February', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'November', 'Desember'];

        $reservasi = new Reservasi();

        $statistic = [
            'bulan' => [],
            'jumlah' => []
        ];

        //menghitung jumlah pengunjung perbulan
        for($i = 1; $bulan >= $i; $i++) {
            array_push($statistic['bulan'], $namabulan[$i-1]);
            $jumlah = $reservasi::select('tanggal')->where('tanggal','like',$tahun.'-0'.$i.'%')->count();
            array_push($statistic['jumlah'], $jumlah);
        }

        //jumlah terbooking
        $terbooking = $reservasi::where('tanggal', $today->format('Y-m-d'))->count();
        //jumlah sisa avaible

        //jumlah total room
        $stok = Ruangan
        ::where('status', 'open')
        ->count();

        return view('dashboard')->with(Compact('pagetitle', 'statistic', 'stok', 'terbooking'));
    }
}