<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Slot;
use App\Reservasi;

class Kelas extends Model
{
    protected $fillable = ['nama', 'stok'];

    public function limit() {
        return $this->stok * Slot::all()->count();
    }

    public function isAvaible() {

        //if($this-limit == )
    }

    function setTanggal($value) {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function reservasi() {
        return $this::hasMany(Reservasi::class);
    }

    public function booked_count($tanggal) {

        return $this::reservasi()
        ->where(
            'status', '1'
        )
        ->count();

    }

    public function booked_slot($tanggal) {
        $param = [
            'kelas_id' => $this->id,
            'tanggal' => $this::setTanggal($tanggal),
            'stok_number' => $this->select_stok($tanggal)
        ];

        return $this::reservasi()
        ->where(
            $param
        )
        ->pluck('slot_id')->toArray();
        //->get();
    }


    public function avaible_time($tanggal) {

        $param = null;

        $booked_slot = $this::booked_slot($tanggal);

        if( $booked_slot != null) {
            $param = $booked_slot;
        }

        $start_time = Slot::where('id', '!=', $param)->pluck('start_time')->toArray();

        //return $start_time;
        return $this->max_hours($booked_slot, $start_time);
    }

    //fungsi ini untuk milihkan stok agak seimbang pada pemilihan slot waktu
    public function select_stok($tanggal) {
        return ($this->booked_count($tanggal) % $this->stok) + 1;
    }

    //fungsi untuk menghitung maximum lama sewa
    public function max_hours($booked, $avaible) {
        $max_hours = [];
        $max_hours_temp = [];

        $pembalik = "";
        $isFinded = false;

        $max_hour = 0; //in unit table
        foreach($avaible as $time1) {

            $tm_avaible = Carbon::parse($time1);

            foreach($booked as $time2) {

                $book = Slot::find($time2);
                $tm_booked = Carbon::parse($book->start_time);
                
                //jika waktu yang tersedia lebih besar dari 
                if($tm_avaible->gt($tm_booked)) {
                    //benar atau lebih besar

                    if($pembalik == 'lebih kecil') {
                        $max_hour = 0 ;

                        array_push($max_hours, array_reverse($max_hours_temp));

                    } 

                    $isFinded = false;

                    $pembalik = 'lebih besar';

                } else {
                    //salah atau lebih kecil

                    if($pembalik == 'lebih besar') {

                        $max_hour = 0 ;

                        array_push($max_hours, array_reverse($max_hours_temp));
                    }

                    if( $isFinded != 'true') {

                        $isFinded = true;
                        //print_r('lebih kecil');
                        $max_hour = $max_hour+1;

                        array_push($max_hours_temp, $max_hour);

                    }

                    $pembalik = 'lebih kecil';

                }


            }

            /* end iteration 2 */
            if($isFinded == 'false') {

                $max_hour = $max_hour+1;

                print_r($max_hour);
                array_push($max_hours, $max_hour);
                
            } else {

                $isFinded = 'false';

            }

        }

        //pembalik nilai maksimasl sewa
        $max_hours_reverse = array_reverse($max_hours);

        // compact data
        $schedule = [
            'booked_time' => $booked,
            'avaible_time' => $avaible,
            'max_hours' => $max_hours_reverse,
        ];

        return $schedule;
    }

}