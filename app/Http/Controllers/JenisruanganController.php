<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jenisruangan;
use Session;
use Illuminate\Support\Facades\File; 

class JenisruanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pagetitle = 'Jenis Ruangan';
        $jenisruangan = Jenisruangan::orderBy('id_jenis_ruangan')->paginate(10);

        return view('jenisruangan.index')->with(compact('pagetitle', 'jenisruangan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pagetitle = 'Jenis Ruangan';
        return view('jenisruangan.new-jenisruangan')->with(compact('pagetitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_jenis_ruangan' => 'required|string|max:30|unique:jenis_ruangan,nama_jenis_ruangan',
            'harga' => 'required|numeric',
	        'foto' => 'required|max:2048',
	        'keterangan' => 'required|string',
        ]);

        $jenisruangan = Jenisruangan::create($request->except('foto'));

        if($request->hasFile('foto')) {
            $uploadfoto = $request->file('foto');

            //ambil extension file
            $extension = $uploadfoto->getClientOriginalExtension();

            //membuat nama file
            $namefile = md5(time()). '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';

            $uploadfoto->move($destinationPath, $namefile);

            $jenisruangan->foto = $namefile;

            $jenisruangan->save();
        }

        Session::flash('flash_notification', [
            'level' => 'success',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => $jenisruangan->nama_jenis_ruangan . ' Berhasil disimpan',
        ]);


        return redirect()->route('jenisruangan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenisruangan = Jenisruangan::findOrFail($id);

        return view('jenisruangan.edit-jenisruangan')->with(compact('jenisruangan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_jenis_ruangan' => 'required|string|max:30|unique:jenis_ruangan,nama_jenis_ruangan,'.$id.',id_jenis_ruangan',
            'harga' => 'required|numeric',
	        'keterangan' => 'required|string',
        ]);

        $jenisruangan = Jenisruangan::find($id);
        $jenisruangan->update($request->except('foto'));

        if($request->hasFile('foto')) {
            $namefile = null;
            $uploadfoto = $request->file('foto');

            //ambil extension file
            $extension = $uploadfoto->getClientOriginalExtension();

            //membuat nama file
            $namefile = md5(time()). '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';

            $uploadfoto->move($destinationPath, $namefile);

            //menghapus file lama jika ada
            if($jenisruangan->foto) {
                $old_foto = $jenisruangan->foto;

                $filepath = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $jenisruangan->foto;

                try {
                    File::delete($filepath);
                } catch(FileNotFoundException $e) {
                    // File sudah di hapus/tidak ada
                }
            }

            //ganti file baru
            $jenisruangan->foto = $namefile;

            $jenisruangan->update();
        }

        Session::flash('flash_notification', [
            'level' => 'success',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => $jenisruangan->nama_jenis_ruangan . 'Berhasil disimpan',
        ]);


        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenisruangan = Jenisruangan::findOrFail($id);

        if($jenisruangan->foto) {

            $old_foto = $jenisruangan->foto;

            $filepath = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $jenisruangan->foto;

            try {
                File::delete($filepath);
            } catch(FileNotFoundException $e) {
                // File sudah di hapus/tidak ada
            }
        }

        $jenisruangan->delete();

        Session::flash('flash_notification', [
            'level' => 'success',
            'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil dihapus',
        ]);


        return redirect()->route('jenisruangan.index');
    }
}
