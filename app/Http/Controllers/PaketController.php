<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use File;
use App\Paketruangan;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagetitle = 'Paket dan promo';
        $paket = Paketruangan::orderBy('id_paket')->paginate(10);

        return view('paket.index')->with(compact('pagetitle', 'paket'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pagetitle = 'Paket dan promo';
        return view('paket.new-paket')->with(compact('pagetitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_paket' => 'required|string|max:30|unique:paket_ruangan,nama_paket',
            'harga_paket' => 'required|numeric',
	        'foto_paket' => 'required|max:2048',
	        'ket_paket' => 'required|string',
        ]);

        $paket = Paketruangan::create($request->except('foto_paket'));

        if($request->hasFile('foto_paket')) {
            $uploadfoto = $request->file('foto_paket');

            //ambil extension file
            $extension = $uploadfoto->getClientOriginalExtension();

            //membuat nama file
            $namefile = md5(time()). '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';

            $uploadfoto->move($destinationPath, $namefile);

            $paket->foto_paket = $namefile;

            $paket->save();
        }

        Session::flash('flash_notification', [
            'level' => 'success',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => $paket->nama_paket . ' Berhasil disimpan',
        ]);


        return redirect()->route('paket.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paket = Paketruangan::findOrFail($id);
        $pagetitle = 'Paket dan promo';
        return view('paket.edit-paket')->with(compact('paket','pagetitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nama_paket' => 'required|string|max:30|unique:paket_ruangan,nama_paket,'.$id.',id_paket',
            'harga_paket' => 'required|numeric',
	        'ket_paket' => 'required|string',
        ]);

        $paket = Paketruangan::find($id);
        $paket->update($request->except('foto_paket'));

        if($request->hasFile('foto_paket')) {
            $namefile = null;
            $uploadfoto = $request->file('foto_paket');

            //ambil extension file
            $extension = $uploadfoto->getClientOriginalExtension();

            //membuat nama file
            $namefile = md5(time()). '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';

            $uploadfoto->move($destinationPath, $namefile);

            //menghapus file lama jika ada
            if($paket->foto_paket) {
                $old_foto = $paket->foto_paket;

                $filepath = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $paket->foto_paket;

                try {
                    File::delete($filepath);
                } catch(FileNotFoundException $e) {
                    // File sudah di hapus/tidak ada
                }
            }

            //ganti file baru
            $paket->foto_paket = $namefile;

            $paket->update();
        }

        Session::flash('flash_notification', [
            'level' => 'success',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => $paket->nama_paket . 'Berhasil disimpan',
        ]);


        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paket = Paketruangan::findOrFail($id);

        if($paket->foto_paket) {

            $old_foto = $paket->foto_paket;

            $filepath = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $paket->foto_paket;

            try {
                File::delete($filepath);
            } catch(FileNotFoundException $e) {
                // File sudah di hapus/tidak ada
            }
        }

        $paket->delete();

        Session::flash('flash_notification', [
            'level' => 'warning',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => $paket->nama_paket . ' Berhasil dihapus',
        ]);

        return redirect()->route('paket.index');
    }
}
