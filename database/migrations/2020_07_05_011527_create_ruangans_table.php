<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRuangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruangan', function (Blueprint $table) {
            $table->id('id_ruangan');
	        $table->string('nama_ruangan');
	        $table->string('nomor');
	        $table->string('status');
	        $table->bigInteger('id_jenis_ruangan')->unsigned();
            $table->timestamps();

	        $table->foreign('id_jenis_ruangan')->references('id_jenis_ruangan')->on('jenis_ruangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ruangan');
    }
}
