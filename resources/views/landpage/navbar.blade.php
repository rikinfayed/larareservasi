<nav class="navbar navbar-expand-lg navbar-light bg-transparent">
    <div class="container">

        <a class="navbar-brand" href="{{ url('/') }}">
            <!--img src="/images/happy-puppy-logo.png" width="187"-->
            <h2 class="herobwa mb-0 d-none d-sm-block">{{ config('app.name') }}</h2>
            <!-- a href="{{ route('book') }}" class="btn btn-hero" role="button">Book Now</a-->
            <!-- p class="herobwa mb-0 d-xs-block d-sm-none">Text</p -->
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav @if(isset($auth_hide) && $auth_hide == true) {{ 'ml-auto' }} @else {{ 'm-auto' }} @endif">
                <li class="nav-item @if(isset($active_menu)) {{ 'active' }} @else {{ '' }} @endif align-self-center">
                    <a class="nav-link" href="#">Pemesanan<span class="sr-only">(current)</span></a>
                </li>
                <!--li class="nav-item align-self-center">
                    <a class="nav-link" href="#">Price</a>
                </li>
                <li class="nav-item align-self-center">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Promo</a>
                </li-->
            </ul>
            @if(isset($auth_hide) && $auth_hide == true)

            @else
            <ul class="navbar-nav mr-0">
                <li class="nav-item active align-self-center">
                    <a class="nav-link" href="#">Dashboard<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item align-self-center">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Masuk') }}</a>
                    <!--a class="btn btn-sm btn-primary" href="#" role="button">Login</a -->
                    <!--a class="btn btn-primary" href="#" tabindex="-1" aria-disabled="true">Login</a-->
                </li>
            </ul>
            @endif
        </div>
    </div>
</nav>