<?php

use Illuminate\Database\Seeder;
use App\Pendidikan;

class PendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $pendidikan = [
            'Tidak Sekolah',
            'Putus SD',
            'SD Sederajat',
            'SMP Sederajat',
            'SMA Sederajat',
            'D1',
            'D2',
            'D3',
            'D4',
            'S1',
            'D4/S1',
            'S2',
            'S3'
        ];

        for($i = 0; $i < count($pendidikan); $i++) {
            $pddk = new Pendidikan();
            $pddk->nama = $pendidikan[$i];
            $pddk->save();
        }
    }
}
