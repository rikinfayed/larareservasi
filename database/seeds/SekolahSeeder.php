<?php

use Illuminate\Database\Seeder;
use App\Sekolahasl;

class SekolahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sekolah = new Sekolahasl();
        $sekolah->nama = 'TK';
        $sekolah->save();
        $sekolah = new Sekolahasl();
        $sekolah->nama = 'PAUD';
        $sekolah->save();
        $sekolah = new Sekolahasl();
        $sekolah->nama = 'RA';
        $sekolah->save();
        $sekolah = new Sekolahasl();
        $sekolah->nama = 'SD';
        $sekolah->save();
        $sekolah = new Sekolahasl();
        $sekolah->nama = 'TIDAK ADA';
        $sekolah->save();
        $sekolah = new Sekolahasl();
        $sekolah->nama = 'LAINNYA';
        $sekolah->save();
    }
}
