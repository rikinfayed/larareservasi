@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
               <div class="card card-info">
                   <div class="card-header">
                        <a href="{{ route('kelurahan.index') }}">Data Kelurahan </a> > Import Kelurahan
                   </div>
                   <div class="card-body">
                        {!! Form::open(['url'=> route('kelurahan.importpost.excel'),'class'=>'form-horizontal', 'metode'=>'post','files'=>'true','enctype'=>'multipart/form-data'])!!}
                                @include('kelurahan._import-kelurahan')
                                <div class="offset-md-2">
                                    {{ Form::submit('Import', ['class'=>'btn btn-primary'])}}
                                    <a href="{{ route('kelurahan.index') }}" class="btn btn-info">Kembali</a>
                                </div>
                            </div>
                        {!! Form::close()!!}
                   </div>
               </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="http://localhost:8000/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-mask]').inputmask('9999');
        });
        $(function() {
            $('.tandawarna').colorpicker();
        });
    </script>
@endsection
