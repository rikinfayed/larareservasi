<section class="formstepreg mt-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-transparent pb-0">
                        <h3 class="text-center mb-3">Form Reservasi Room</h3>
                        <div class="row">
                            <div class="col text-center steped pb-2">
                                <p class="mb-0">Identitas Anda</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Pilih Hari</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Checkout</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body mt-4">
                        <div class="row">
                            <div class="col-4">
                                <h4>Informasi</h4>
                                <ul>
                                    <li><p>Pastikan data terinput dengan benar</p></li>
                                </ul>
                            </div>
                            <div class="col-8">
                                <form>
                                    <div class="form-group">
                                        {!! Form::label('nama', 'Nama Lengkap', ['class' => '']) !!}
                                        {!! Form::text('nama', null, ['class'=> $errors->has('nama') ? 'form-control is-invalid': 'form-control', 'autofocus']) !!}
                                        {!! $errors->first('nama', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('jenis_kelamin', 'Jenis Kelamin', ['class' => '']) !!}
                                        <div class="form-inline {{ $errors->has('jenis_kelamin') ? 'is-invalid' : ''}} ">
                                            {!! Form::radio('jenis_kelamin','LAKI-LAKI', null,['class'=>'form-check-input mx-3 mb-3']) !!}
                                                <p>Laki-Laki</p>
                                            {!! Form::radio('jenis_kelamin','PEREMPUAN', null,['class'=>'form-check-input mx-3 mb-3']) !!}
                                                <p>Perempuan</p>
                                        </div>
                                        {!! $errors->first('jenis_kelamin', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('nomor_telepon', 'Nomor Hp', ['class'=> '']) !!}
                                        {!! Form::text('nomor_telepon', null, ['class'=>$errors->has('nomor_telepon')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('nomor_telepon', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('email', 'Email', ['class'=> '']) !!}
                                        {!! Form::text('email', null, ['class'=>$errors->has('email')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('email', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-center">
                                <li class="page-item">
                                    <a href="{{route('book.cancel')}}" class="page-link btn-danger">Batal</a>
                                </li>
                                <li class="page-item">
                                    {!! Form::submit('Simpan & Lanjutkan', ['class'=> 'page-link btn-primary']) !!}
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>