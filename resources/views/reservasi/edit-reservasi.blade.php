@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
               <div class="card card-info">
                   <div class="card-header">
                        <h3 class="card-title">Form New Reservasi</h3>
                   </div>
                   <div class="card-body">
                        {!! Form::model($reservasi,['url'=> route('reservasi.update', $reservasi->id_reservasi),'method'=>'put','class'=>'form-horizontal'])!!}
                            @include('reservasi._edit-reservasi')
                            <div class="col-md-4">
                                {{ Form::submit('Simpan', ['class'=>'btn btn-primary'])}}
                                <a href="{{ route('reservasi.index') }}" class="btn btn-info">Kembali</a>
                            </div>
                        {!! Form::close()!!}
                   </div>
               </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="http://localhost:8000/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-mask]').inputmask('9999');
        });
    </script>
@endsection
