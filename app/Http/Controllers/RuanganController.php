<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ruangan;
use Session;

class RuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagetitle = 'Ruangan';
        $ruangan = Ruangan::orderBy('id_ruangan')->paginate(10);

        return view('ruangan.index')->with(compact('pagetitle', 'ruangan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pagetitle = 'Data Ruangan';
        return view('ruangan.new-ruangan')->with(compact('pagetitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_ruangan' => 'required|string|max:30',
            'nomor' => 'required|numeric|unique:ruangan,nomor',
            'status' => 'required|string',
            'id_jenis_ruangan' => 'required',
        ]);

        $ruangan = Ruangan::create($request->all());

        Session::flash('flash_notification', [
            'level' => 'success',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => $ruangan->nama_ruangan . ' Berhasil disimpan',
        ]);


        return redirect()->route('ruangan.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ruangan = Ruangan::findOrFail($id);
        $pagetitle = 'Data Ruangan';
        return view('ruangan.edit-ruangan')->with(compact('ruangan','pagetitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'nama_ruangan' => 'required|string|max:30',
            'nomor' => 'required|numeric|unique:ruangan,nomor,'.$id.',id_ruangan',
            'status' => 'required|string',
            'id_jenis_ruangan' => 'required',
        ]);

        $ruangan = Ruangan::findOrFail($id);
        $ruangan->nama_ruangan = $request->get('nama_ruangan');
        $ruangan->nomor = $request->get('nomor');
        $ruangan->status = $request->get('status');
        $ruangan->id_jenis_ruangan = $request->get('id_jenis_ruangan');
        $ruangan->update();

        Session::flash('flash_notification', [
            'level' => 'success',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => $ruangan->nama_ruangan . ' Berhasil disimpan',
        ]);


        return redirect()->route('ruangan.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ruangan = Ruangan::findOrFail($id);

        $ruangan->delete();

        Session::flash('flash_notification', [
            'level' => 'warning',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => $ruangan->nama_ruangan . ' Berhasil dihapus',
        ]);


        return redirect()->route('ruangan.index');
    }
}
