<section class="formstepreg mt-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-transparent pb-0">
                        <h3 class="text-center mb-3">Form Reservasi Room</h3>
                        <div class="row">
                            <div class="col text-center steped pb-2">
                                <p class="mb-0">Identitas Anda</p>
                            </div>
                            <div class="col text-center steped pb-2">
                                <p class="mb-0">Pesan Ruangan</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Checkout</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body mt-4">
                        <div class="row">
                            <div class="col-4">
                                <h4>Informasi</h4>
                                <ul>
                                    <li><p>Pastikan data terinput dengan benar</p></li>
                                </ul>
                            </div>
                            <div class="col-8">
                                <form>
                                    <div class="form-group ">
                                        {{ Form::label('id_jenis_ruangan', 'Jenis Ruangan', ['class'=>'control-label col-md-4']) }}
                                        {{ Form::select(
                                                    'id_jenis_ruangan',
                                                    ['' => '== Pilihan ==']+App\Jenisruangan::pluck('nama_jenis_ruangan', 'id_jenis_ruangan')->all(),
                                                    null,
                                                    ['class'=> $errors->has('id_jenis_ruangan') ? ' form-control is-invalid' : 'form-control' ]) }}
                                        {!! $errors->first('id_jenis_ruangan', '<p class="help-block">:message<p>') !!}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::label('id_paket', 'Paket/Promo', ['class'=>'control-label col-md-4']) }}
                                        {{ Form::select(
                                            'id_paket',
                                            ['' => '== Pilihan ==']+App\Paketruangan::pluck('nama_paket', 'id_paket')->all(),
                                            null,
                                            ['class'=> $errors->has('id_paket') ? 'form-control is-invalid' : 'form-control'])
                                        }}
                                        {!! $errors->first('id_paket', '<p class="help-block">:message<p>') !!}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label(
                                            'jam_rsv',
                                            'Jam Reservasi',
                                            ['class'=>'control-label col-md-4'])
                                            }}
                                        {{ Form::time('jam_rsv', null, ['class'=> $errors->has('jam_rsv')?  'form-control is-invalid': 'form-control']) }}
                                        {!! $errors->first('jam_rsv', '<p class="help-block">:message<p>') !!}
                                    </div>
                                    <?php
                                        $lama_sewa = [
                                            '1' => '1 Jam',
                                            '2' => '2 Jam',
                                            '3' => '3 Jam',
                                            '4' => '4 Jam',
                                            '5' => '5 Jam',
                                            '6' => '6 Jam',
                                        ];
                                    ?>
                                    <div class="form-group ">
                                        {{ Form::label('lama_sewa', 'Lama Sewa', ['class'=>'control-label col-md-4']) }}
                                            {{ Form::select('lama_sewa', [''=>'Pilihan']+$lama_sewa,null, ['class'=>$errors->has('lama_sewa') ? ' form-control is-invalid' : 'form-control']) }}
                                            {!! $errors->first('lama_sewa', '<p class="help-block">:message<p>') !!}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::label('tanggal', 'Tanggal Reservasi', ['class'=>'control-label col-md-4']) }}
                                            {{ Form::date('tanggal', null, ['class'=> $errors->has('tanggal') ? 'form-control is-invalid' : 'form-control']) }}
                                            {!! $errors->first('tanggal', '<p class="help-block">:message<p>') !!}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('pesan', 'Note', ['class'=>'control-label col-md-4']) }}
                                        {{ Form::textarea('pesan', null, ['class'=>$errors->has('pesan') ? 'form-control is-invalid' : 'form-control']) }}
                                        {!! $errors->first('pesan', '<p class="help-block">:message<p>') !!}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-center">
                                <li class="page-item">
                                    <a href="{{route('book.cancel')}}" class="page-link btn-danger">Batal</a>
                                </li>
                                <li class="page-item">
                                    {!! Form::submit('Simpan & Lanjutkan', ['class'=> 'page-link btn-primary']) !!}
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>