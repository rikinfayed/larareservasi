<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    @guest
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custome.css') }}" rel="stylesheet">
    @else
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('adminlte/dist/css/adminlte.css') }}" rel="stylesheet">
    @endguest
</head>
<body>
    <div id="app">
            @guest
                    @include('landpage.navbar')
                    @include('layouts._flashnoadmin')
            @else
                @if(!Laratrust::hasRole('admin'))
                    <!--('operator.navbar') -->
                    @include('layouts.navbar')
                    @include('layouts.sidebar')
                    @include('layouts.header-content')
                    @include('layouts._flashnoadmin')
                @else
                    <!-- admin -->
                    @include('layouts.navbar')
                    @include('layouts.sidebar')
                    @include('layouts.header-content')
                    @include('layouts._flash')
                @endif
            @endguest

            <main class="py-4">
                @yield('content')
            </main>

            @include('layouts.close-content')
    </div>
     <!-- REQUIRED SCRIPTS -->
     <!-- jQuery -->
     <script src="{{ asset('js/jquery.min.js') }}"></script>
     <!-- Bootstrap 4 -->
     <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
     <!-- AdminLTE App -->
     <script src="{{ asset('js/adminlte.min.js') }}"></script>
     <script src="{{ asset('js/select2.full.min.js') }}"></script>
     <!-- bootstrap color picker -->
     <script src="{{ asset('js/bootstrap-colorpicker.min.js') }}"></script>
     <!-- OPTIONAL SCRIPTS -->
     <script src="{{ asset('js/Chart.min.js') }}"></script>
     <script src="{{ asset('js/demo.js') }}"></script>

     @yield('scripts')
</body>
</html>
