<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaketruangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paket_ruangan', function (Blueprint $table) {
            $table->id('id_paket');
            $table->string('nama_paket');
            $table->integer('harga_paket');
            $table->string('foto_paket')->nullable();
            $table->string('ket_paket');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paket_ruangan');
    }
}
