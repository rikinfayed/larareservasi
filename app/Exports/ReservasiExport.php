<?php

namespace App\Exports;

use App\Reservasi;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;


class ReservasiExport implements FromQuery, WithHeadings
{
    public function __construct($start_date, $end_date) {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    public function query () {
        return Reservasi::query()->whereBetween('tanggal',[$this->start_date, $this->end_date]);
    }

    public function headings(): array
    {
        return [
            'Tanggal',
            'Nama',
            'Gender',
            'Jam',
            'Lama Sewa',
            'Nomor HP',
            'Ruangan',
            'Paket',
            'Note'
        ];
    }
}
