@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header d-flex p-0">
                    <h3 class="card-title p-3">Daftar Reservasi</h3>
                    <ul class="nav nav-pills ml-auto p-2">
                        <li class="nav-item"><a class="nav-link" href="{{route('reservasi.create')}}"><i class="fa fa-plus"></i> Tambah Reservasi</a></li>
                        <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" >
                                    <i class="fa fa-bars"></i>
                                </a>
                                <div class="dropdown-menu">
                                    <!--a class="dropdown-item" tabindex="-1" href="#" style="color:black">
                                        <i class="fa fa-refresh fa-spin"></i>
                                        Action
                                        <span class="float-right text-muted text-sm"></span>
                                    </a-->
                                    <div class="divider"></div>
                                    <!--a class="dropdown-item" tabindex="-1" href="route('kelurahan.import.excel')"-->
                                    <a class="dropdown-item" tabindex="-1" href="#">
                                        <i class="fa fa-upload" style="color:black;"></i>
                                        <span style="color:black">Impor dari Excel</span>
                                        <span class="float-right text-muted text-sm"></span>
                                    </a>
                                    <!--a class="dropdown-item" tabindex="-1" href="route('kelurahan.export.excel')"-->
                                    <a class="dropdown-item" tabindex="-1" href="#">
                                        <i class="fa fa-file" style="color:black;"></i>
                                        <span style="color:black">Ekspor Data Kelurahan ke Excel</span>
                                        <span class="float-right text-muted text-sm"></span>
                                    </a>
                                </div>
                        </li> 
                    </ul>
                </div>
                <div class="card-body">
                    {!! Form::open(['url'=>route('reservasi.filter'),'method'=>'get']) !!}

                    <div class="row">
        				<div class="form-group row col-md-6">
        					<label for="start" class="col-form-label col-md-4">Tanggal Awal</label>
        					<div class="col-md-8 input-group">
        						<span class="input-group-prepend">
        							<span class="input-group-text"><i class="fa fa-calendar"></i></span>
        						</span>
        						<input class="form-control" tanggal-mask name="start" type="date" id="start" value="{{ (isset($start)) ? $start: '' }}">
        					</div>
        				</div>
        				<div class="form-group row col-md-6">
        					<label for="end" class="col-form-label col-md-4">Tanggal Akhir</label>
        					<div class="col-md-8 input-group">
        						<span class="input-group-prepend">
        							<span class="input-group-text"><i class="fa fa-calendar"></i></span>
        						</span>
        						<input class="form-control" tanggal-mask name="end" type="date" id="end" value="{{ (isset($end)) ? $end: '' }}">
        					</div>
        				</div>
        			</div>
		        	<div class="row">
		        		<div class="form-group row col-md-6">
		        		</div>
		        		<div class="form-group row col-md-6">
		        			<div class="col-md-8 offset-md-4 input-group">
		        				<input class="btn btn-primary" type="submit" value="Tampilkan">
		        				<button class="ml-3 btn btn-success" id="exportexcel">Export Ke Excel</button>
		        			</div>
		        		</div>
		        	</div>
                    {!! Form::close() !!}
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th> 
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                    <th>Gender</th>
                                    <th>Jam</th>
                                    <th>Lama Sewa</th>
                                    <th>Nomor HP</th>
                                    <th>Ruangan</th>
                                    <th>Paket</th>
                                    <th>Note</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($reservasi) && $reservasi->count() > 0)
                                <?php $i = $reservasi->FirstItem(); ?>
                                @foreach($reservasi as $rservasi)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $rservasi->tanggal }}</td>
                                        <td>{{ $rservasi->nama }}</td>
                                        <td>{{ $rservasi->jenis_kelamin }}</td>
                                        <td>{{ $rservasi->jam_rsv }}</td>
                                        <td>{{ $rservasi->lama_sewa }}</td>
                                        <td>{{ $rservasi->nomor_telepon }}</td>
                                        <td>{{ $rservasi->jenisruangan()->nama_jenis_ruangan }}</td>
                                        <td>{{ $rservasi->paketruangan()->nama_paket }}</td>
                                        <td>{{ $rservasi->pesan }}</td>
                                        <td>
                                            @if( $rservasi->flag_status != 1) <a href="{{ route('reservasi.arrived', $rservasi->id_reservasi) }}"class="btn-success btn-sm">Datang</a> 
                                            @else
                                                {{ __('Datang')}}
                                            @endif
                                            @if( $rservasi->flag_status != 1)
                                            <a href="{{ route('reservasi.edit', $rservasi->id_reservasi) }}"class="btn-info btn-sm">Edit</a>
                                            <a href="#" class="btn-danger btn-sm" onclick=" var check = confirm('Yakin Hapus {{ $rservasi->id_reservasi }}?'); if(check) {event.preventDefault();document.getElementById('formdelete{{ $rservasi->id_reservasi }}').submit();}" >Batal</a>
                                            {{ Form::open(['url'=>route('reservasi.destroy', $rservasi->id_reservasi), 'method'=>'delete', 'id'=>'formdelete'.$rservasi->id_reservasi, 'class'=>'form-inline', 'data-confirm'=>'Yakin Hapus '.$rservasi->id_reservasi])}}
                                            {{ Form::close() }}
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                @else
                                    <tr><td colspan="11" class="text-center"> Data Kosong </td></tr>
                                @endif
                            </body>
                        </table>
                    </div>
                    <div class="float-left pagination-md mb-0 clear-fix">
                          @if(isset($reservasi) && $reservasi->count() > 0)
                            Menampilkan {{ $reservasi->FirstItem() }} sampai {{$reservasi->LastItem()}} Dari {{$reservasi->total()}} Entri
                          @endif
                    </div>
                    <div class="float-right pagination-md mb-0 clear-fix">
                          @if(isset($reservasi) && $reservasi->count() > 0)
                            @if(isset($start) && isset($end))
                                {{$reservasi->appends(['start'=>$start, 'end'=>$end])->links()}}
                            @else
                                {{$reservasi->links()}}
                            @endif
                          @endif
                    </div>
                </div>
                <!--div class="overlay preload">
                    <i class="fa fa-refresh fa-spin"></i>
                </div-->
            </div>
        </div>

        <!--div class="card col-md-4">
            <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg">$18,230.00</span>
                    <span>Sales Over Time</span>
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-success">
                      <i class="fa fa-arrow-up"></i> 33.1%
                    </span>
                    <span class="text-muted">Since last month</span>
                  </p>
                </div>


                <div class="position-relative mb-4">
                  <canvas id="sales-chart" height="200"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fa fa-square text-primary"></i> This year
                  </span>

                  <span>
                    <i class="fa fa-square text-gray"></i> Last year
                  </span>
                </div>
            </div>
        </div-->

    </div>
        <!-- iframe class="container-fluid" height="800px" src="{{url('print/pdf')}}"></iframe-->
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.preload').fadeOut();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });


        $('#exportexcel').click(function (e) {
            e.preventDefault();

            var link = "{{route('reservasi.export') }}";
            var start = $('#start').val();
            var end = $('#end').val();

            $.ajax({
                cache: false,
                type:'POST',
                url:link,
                data:{start: start, end: end, _token: '{{ csrf_token() }}'},
                success:function(response, textStatus, request){
                    var a = document.createElement("a");
                    a.href = response.file; 
                    a.download = response.name;
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                },
                error: function (ajaxContext) {
                    toastr.error('Export error: '+ajaxContext.responseText);
                }
            });
            /*
            var query = {
                start: start,
                end: end,
                _token : '{{csrf_token() }}'
            }


            var url = link + $.param(query);

            window.location = url;
            */
        });

    </script>
@endsection