<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ruangan;

class Jenisruangan extends Model
{
    protected $table = 'jenis_ruangan';

    public $primaryKey = 'id_jenis_ruangan';

    protected $fillable = [
        'nama_jenis_ruangan',
        'harga',
        'foto',
        'keterangan'
    ];

    public function ruangan() {
        return Ruangan::where('id_jenis_ruangan', $this->id_jenis_ruangan);
    }
}
