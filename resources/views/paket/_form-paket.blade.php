<div class="form-group {{ $errors->has('nama_paket') ? ' has-error' : ''}}">
    {{ Form::label('nama_paket', 'Nama Paket', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('nama_paket', null, ['class'=>'form-control']) }}
        {!! $errors->first('nama_paket', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('harga_paket')? ' has-error' : ''}}">
    {{ Form::label('harga_paket', 'Harga', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('harga_paket', null, ['class'=>'form-control', 'data-mask'])}}
        {!! $errors->first('harga_paket', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('foto_paket') ? ' has-error' : ''}}">	
    {{ Form::label('foto_paket', 'Image (Akan ditampilkan di Menu Promo)', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
	{{ Form::file('foto_paket', ['class'=>''])}}
	{!! $errors->first('foto_paket', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    @if(isset($paket) && $paket->foto_paket)
	<image src="{{ asset('img/'.$paket->foto_paket) }}" class="img_rounded img-fluid">
    @endif
</div>
<div class="form-group {{ $errors->has('ket_paket') ? ' has-error' : ''}}">
    {{ Form::label('ket_paket', 'Keterangan', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::textarea('ket_paket', null, ['class'=>'form-control']) }}
        {!! $errors->first('ket_paket', '<p class="help-block">:message<p>') !!}
    </div>
</div>
