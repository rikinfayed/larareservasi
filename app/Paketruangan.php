<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paketruangan extends Model
{
    public $table = 'paket_ruangan';

    public $primaryKey = 'id_paket';

    protected $fillable = [
        'nama_paket',
        'harga_paket',
        'foto_paket',
        'ket_paket',
    ];
}
