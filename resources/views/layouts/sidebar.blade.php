  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
      <img src="{{ asset('adminlte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{ config('app.name') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!--img src="{{ asset('adminlte/dist/img/avatar.png') }}" class="img-circle elevation-2" alt="User Image"-->
        </div>
        <div class="info nav-item">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{route('home') }}" class="nav-link @if(isset($pagetitle) && $pagetitle=='Dashboard') {{ __('active') }} @else {{ __('') }} @endif">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @role('admin')
          @if(Route::has('jenisruangan.index'))
          <li class="nav-item">
            <a href="{{ route('jenisruangan.index') }}" class="nav-link @if(isset($pagetitle) && $pagetitle=='Jenis Ruangan') {{ __('active') }} @else {{ __('') }} @endif">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Jenis Ruangan
              </p>
            </a>
          </li>
          @endif
          @if(Route::has('ruangan.index'))
          <li class="nav-item">
            <a href="{{ route('ruangan.index') }}" class="nav-link @if(isset($pagetitle) && $pagetitle=='Ruangan') {{ __('active') }} @else {{ __('') }} @endif">
              <i class="nav-icon fa fa-clone"></i>
              <p>
                Stok Ruangan
              </p>
            </a>
          </li>
          @endif
          @if(Route::has('paket.index'))
          <li class="nav-item">
            <a href="{{ route('paket.index') }}" class="nav-link @if(isset($pagetitle) && $pagetitle=='Paket dan promo') {{ __('active') }} @else {{ __('') }} @endif">
              <i class="nav-icon fa fa-percent"></i>
              <p>
                Promo dan Paket
              </p>
            </a>
          </li>
          @endif
        @endrole
          @if(Route::has('reservasi.index'))
          <li class="nav-item">
            <a href="{{ route('reservasi.index') }}" class="nav-link @if(isset($pagetitle) && $pagetitle=='Reservasi') {{ __('active') }} @else {{ __('') }} @endif">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Reservasi
              </p>
            </a>
          </li>
          @endif
          @role('admin')
          <li class="nav-item">
            <a href="{{route('user.index')}}" class="nav-link @if(isset($pagetitle) && $pagetitle=='Manage Accounts') {{ __('active') }} @else {{ __('') }} @endif">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Manage Accounts
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          @endrole
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link"
              onclick="event.preventDefault();document.getElementById('logout-form').submit()"
            >
              <i class="nav-icon fa fa-sign-out" style="color:red;"></i>
              <p>
                {{ __('Logout') }}
              </p>
              {{ Form::open(['url'=>route('logout'),'method'=>'post','style'=>'display:none', 'id'=>'logout-form'])}}
              {{ Form::close() }}
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>