@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex p-0">
                    <div class="card-title p-3">Daftar Kecamatan</div>
                    <ul class="nav nav-pills ml-auto p-2">
                        <li class="nav-item"><a class="nav-link active" href="{{route('kecamatan.create')}}">Tambah Kecamatan</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Kecamatan</th>
                                    <th>Kode Kecamatan</th>
                                    <th>Luas Wilayah</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($kec) && $kec->count() > 0)
                                <?php $i = $kec->FirstItem(); ?>
                                @foreach($kec as $kc)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $kc->nama }}</td>
                                        <td>{{ $kc->kode }}</td>
                                        <td>{{ $kc->luas }}</td>
                                        <td>
                                            <a href="{{ route('kecamatan.edit', $kc->id) }}"class="btn-info btn-sm">Edit</a>
                                            <a href="{{ route('kecamatan.destroy', $kc->id) }}"class="btn-danger btn-sm" onclick="event.preventDefault();document.getElementById('formdelete{{ $kc->id }}').submit()">Hapus</a>
                                            {{ Form::open(['url'=>route('kecamatan.destroy', $kc->id), 'method'=>'delete', 'style'=>'display:none;', 'id'=>'formdelete'.$kc->id])}}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                @else
                                    <tr><td colspan="5"> Data Kosong </td></tr>
                                @endif
                            </body>
                        </table>
                    </div>
                    <div class="float-left pagination-md mb-0 clear-fix">
                            Menampilkan {{ $kec->FirstItem() }} sampai {{$kec->LastItem()}} Dari {{$kec->total()}} Entri
                    </div>
                    <div class="float-right pagination-md mb-0 clear-fix">
                            {{$kec->links()}}
                    </div>
                </div>
                <div class="overlay preload">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>

        <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg">$18,230.00</span>
                    <span>Sales Over Time</span>
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-success">
                      <i class="fa fa-arrow-up"></i> 33.1%
                    </span>
                    <span class="text-muted">Since last month</span>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-4">
                  <canvas id="sales-chart" height="200"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fa fa-square text-primary"></i> This year
                  </span>

                  <span>
                    <i class="fa fa-square text-gray"></i> Last year
                  </span>
                </div>
              </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.preload').fadeOut();
        });
    </script>
@endsection