<?php

use Illuminate\Database\Seeder;
use App\Transportasi;

class TransportasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Jalan Kaki',
            'Sepeda',
            'Kereta Api',
            'Kendara Umum (Angkot, Bus)',
            'Jemputan',
            'Ojek',
            'Mobil',
            'Motor'
        ];


        for($i = 0; $i < count($data); $i++) {
            $kendaraan = new Transportasi();
            $kendaraan->nama = $data[$i];
            $kendaraan->save();
        }
    }
}
