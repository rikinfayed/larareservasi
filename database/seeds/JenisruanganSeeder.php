<?php

use Illuminate\Database\Seeder;
use App\Jenisruangan;

class JenisruanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jenis_ruangan = new Jenisruangan();

        $jenis_ruangan::create(['nama_jenis_ruangan'=>'Studio', 'harga'=>'1500', 'foto'=>'path', 'keterangan' => 'tegeranag']);
    }
}
