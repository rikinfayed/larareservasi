@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex p-0">
                    <div class="card-title p-3">Daftar Akun</div>
                    <ul class="nav nav-pills ml-auto p-2">
                        <li class="nav-item"><a class="nav-link active" href="{{route('user.create')}}">Tambah User</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Hak Akses</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($user))
                                <?php $i = $user->FirstItem(); ?>
                                @foreach($user as $usr)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $usr->name }}</td>
                                        <td>{{ $usr->username }}</td>
                                        <td>{{ $usr->roles()->first()->display_name }}</td>
                                        <td>
                                            @if(Auth::user()->name == $usr->name)
                                                {{ __('Status Sedang Login')}}
                                            @else
                                            <a href="{{ route('setpassword', $usr->id) }}"class="btn-warning btn-sm">Ganti Password</a>
                                            <a href="{{ route('user.edit', $usr->id) }}"class="btn-info btn-sm">Edit</a>
                                            <a href="#"class="btn-danger btn-sm" onclick="var check=confirm('Yakin Hapus {{ $usr->name }}'); if(check) {event.preventDefault();document.getElementById('formdeleteuser{{ $usr->id }}').submit();}">Hapus</a>
                                            {{ Form::open(['url'=>route('user.destroy', $usr->id), 'method'=>'delete', 'style'=>'display:none;', 'id'=>'formdeleteuser'.$usr->id])}}
                                            {{ Form::close() }}
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                @else
                                    <tr> Data Kosong </tr>
                                @endif
                            </body>
                        </table>
                    </div>
                    <div class="float-left pagination-md mb-0 clear-fix">
                            Menampilkan {{ $user->FirstItem() }} sampai {{$user->LastItem()}} Dari {{$user->total()}} Entri
                    </div>
                    <div class="float-right pagination-md mb-0 clear-fix">
                            {{$user->links()}}
                    </div>
                </div>
                <div class="overlay preload">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            <div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.preload').fadeOut();
        });
    </script>
@endsection