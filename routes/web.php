<?php

use Illuminate\Support\Facades\Route;
use App\Jenisruangan;
use App\Paketruangan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    $jnsruangans = Jenisruangan::all();
    $pktruangans = Paketruangan::all();

    return view('welcome')->with(compact('jnsruangans', 'pktruangans'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Guesh Order Reservasi
Route::get('/book', 'ReservasiController@reservasi')->name('reservasi');
Route::get('book', 'ReservasiController@book')->name('book');
//Route::get('reservasi/book/page/{page}', 'ReservasiController@bookpage')->name('reservasi.book.page');
Route::post('book/simpan', 'ReservasiController@booksimpan')->name('book.simpan');
Route::get('book/cancel', 'ReservasiController@bookcancel')->name('book.cancel');

// Login First
Route::group(['middleware'=>['auth']], function() {

    Route::get('reservasi/index', 'ReservasiController@index')->name('reservasi.index');
    Route::get('reservasi/filter', 'ReservasiController@filter')->name('reservasi.filter');
    Route::get('reservasi/create', 'ReservasiController@create')->name('reservasi.create');
    Route::get('reservasi/arrived/{id}', 'ReservasiController@arrived')->name('reservasi.arrived');
    Route::post('reservasi', 'ReservasiController@store')->name('reservasi.store');
    Route::get('reservasi/{reservasi}', 'ReservasiController@edit')->name('reservasi.edit');
    Route::put('reservasi/{reservasi}', 'ReservasiController@update')->name('reservasi.update');
    Route::delete('reservasi/{reservasi}', 'ReservasiController@destroy')->name('reservasi.destroy');
    Route::post('reservasi/export', 'ReservasiController@export')->name('reservasi.export');
        
    
    // Only Admin
    Route::group(['middleware'=>['role:admin']], function() {
        Route::resource('jenisruangan', 'JenisruanganController');
        Route::resource('ruangan', 'RuanganController');
        Route::resource('paket', 'PaketController');
                
        //User Management
        Route::resource('user', 'UserController');
        Route::get('user/setpassword/{id}', 'UserController@setpassword')->name('setpassword');
        Route::put('user/updatepassword/{id}', 'UserController@updatepassword')->name('updatepassword');
    });
    
});

    // Checking Session
    Route::get('/check/session', function () {
        return session()->all();
    });