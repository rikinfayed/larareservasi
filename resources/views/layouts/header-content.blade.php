
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="@if(Laratrust::hasRole('admin')) container-fluid @else container @endif">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@if(isset($pagetitle)) {{ $pagetitle }} @else {{ __('') }} @endif</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{!! route('home') !!}">Home</a></li>
              <li class="breadcrumb-item active">@if(isset($pagetitle)) {{ $pagetitle }} @else {{ __('') }} @endif
              </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
