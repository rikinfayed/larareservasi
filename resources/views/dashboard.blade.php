@extends('layouts.app')

@section('content')
    <div class="row">
    <div class="col-md-6">
    <div class="card">
      <div class="card-header no-border">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Statistic Pengunjung Reservasi</h3>
          <a href="javascript:void(0);"></a>
        </div>
        <p class="p-0 m-0">berdasarkan per bulan</p>
      </div>
      <div class="card-body">

        <div class="position-relative mb-4">
          <canvas id="visitors-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <!--<span class="mr-2">
            <i class="fa fa-square text-primary"></i> This Week
          </span>

          <span>
            <i class="fa fa-square text-gray"></i> Last Week
          </span>-->
        </div>
      </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="card">
      <div class="card-header no-border">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Ketersedian Ruangan Saat ini</h3>
              <span class="text-bold text-lg text-right">{{ $stok }} Room</span>
        </div>
      </div>
      <div class="card-body">

        <div class="position-relative mb-4">
          <canvas id="sales-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span class="mr-2">
            <i class="fa fa-square text-primary"></i> Tersedia
          </span>
          <span class="mr-2">
            <i class="fa fa-square text-danger"></i> Terisi
          </span>
        </div>
      </div>
    </div>
    <!-- /.card -->

    </div>
    
    </div>

@endsection

@section('scripts')
<script>
  $(document).ready(function(){
      $('.preload').fadeOut();
  })

  $(document).ready(function () {

  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode      = 'index'
  var intersect = true


  var $salesChart = $('#sales-chart')
  var salesChart  = new Chart($salesChart, {
    type   : 'pie',
    data   : {
      labels  : ['Terisi', 'Tersedia'],
      datasets: [
        {
          backgroundColor: ['#dc3545','#007bbb'],
          borderColor    : ['#dc3545','#007bbb'],
          data           : [{{ $terbooking }}, {{ $stok - $terbooking }}]
        }
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero: true,

            // Include a dollar sign in the ticks
            callback: function (value, index, values) {
              if (value >= 1000) {
        value /= 1000
                value += 'k'
              }
              return value
            }
          }, ticksStyle)
        }],
        xAxes: [{
          display  : false,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })

  var $visitorsChart = $('#visitors-chart')
  var visitorsChart  = new Chart($visitorsChart, {
    data   : {
      labels  : @if(isset($statistic['bulan'])) {!! json_encode($statistic['bulan']) !!}@else [] @endif,//['18th', '20th', '22nd', '24th', '26th', '28th', '30th'],
      datasets: [{
        type                : 'line',
        data                : @if(isset($statistic['jumlah'])) {!! json_encode($statistic['jumlah']) !!}@else [] @endif,//[100, 120, 170, 167, 180, 177, 160],
        backgroundColor     : 'transparent',
        borderColor         : '#007bff',
        pointBorderColor    : '#007bff',
        pointBackgroundColor: '#007bff',
        fill                : false
        // pointHoverBackgroundColor: '#007bff',
        // pointHoverBorderColor    : '#007bff'
      }/*,
        {
          type                : 'line',
          data                : [60, 80, 70, 67, 80, 77, 100],
          backgroundColor     : 'tansparent',
          borderColor         : '#ced4da',
          pointBorderColor    : '#ced4da',
          pointBackgroundColor: '#ced4da',
          fill                : false
          // pointHoverBackgroundColor: '#ced4da',
          // pointHoverBorderColor    : '#ced4da'
        }*/]
 },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            suggestedMax: 200
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })
});
</script>
@endsection