<?php

use Illuminate\Database\Seeder;
use App\Gaji;

class GajiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gaji = new Gaji();
        $gaji->nilai_max = '1000000';
        $gaji->save();

        $gaji = new Gaji();
        $gaji->nilai_min = '1000000';
        $gaji->nilai_max = '2000000';
        $gaji->save();

        $gaji = new Gaji();
        $gaji->nilai_min = '2000000';
        $gaji->nilai_max = '3000000';
        $gaji->save();

        $gaji = new Gaji();
        $gaji->nilai_min = '3000000';
        $gaji->nilai_max = '4000000';
        $gaji->save();

        $gaji = new Gaji();
        $gaji->nilai_min = '4000000';
        $gaji->nilai_max = '5000000';
        $gaji->save();

        $gaji = new Gaji();
        $gaji->nilai_min = '5000000';
        $gaji->save();
    }
}
