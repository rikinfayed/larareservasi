<div class="form-group {{ $errors->has('id_jenis_ruangan') ? ' has-error' : ''}}">
    {{ Form::label('id_jenis_ruangan', 'Jenis Ruangan', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::select('id_jenis_ruangan',['' => '== Pilihan ==']+App\Jenisruangan::pluck('nama_jenis_ruangan','id_jenis_ruangan')->all(), null, ['class'=>'form-control']) }}
        {!! $errors->first('id_jenis_ruangan', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('nama_ruangan') ? ' has-error' : ''}}">
    {{ Form::label('nama_ruangan', 'Nama Ruangan', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('nama_ruangan', null, ['class'=>'form-control']) }}
        {!! $errors->first('nama_ruangan', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('nomor')? ' has-error' : ''}}">
    {{ Form::label('nomor', 'Nomor Ruangan', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('nomor', null, ['class'=>'form-control', 'data-mask'])}}
        {!! $errors->first('nomor', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('status') ? ' has-error' : ''}}">
    {{ Form::label('status', 'Status', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::select('status',['' => '== Pilihan ==', 'close' => 'Tutup','open' => 'Buka'], null, ['class'=>'form-control']) }}
        {!! $errors->first('status', '<p class="help-block">:message<p>') !!}
    </div>
</div>
