<div class="form-group {{ $errors->has('nama_jenis_ruangan') ? ' has-error' : ''}}">
    {{ Form::label('nama_jenis_ruangan', 'Nama', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('nama_jenis_ruangan', null, ['class'=>'form-control']) }}
        {!! $errors->first('nama_jenis_ruangan', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('harga')? ' has-error' : ''}}">
    {{ Form::label('harga', 'Harga', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('harga', null, ['class'=>'form-control', 'data-mask'])}}
        {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('foto')? ' has-error' : ''}}">
    {{ Form::label('foto', 'Image', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::file('foto',['class'=>''])}}
        {!! $errors->first('foto', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    @if(isset($jenisruangan) && $jenisruangan->foto)
        <image src="{{ asset('img/'.$jenisruangan->foto) }}" class="img-rounded img-fluid"/>
    @endif
</div>
<div class="form-group {{ $errors->has('keterangan')? ' has-error' : ''}}">
    {{ Form::label('keterangan', 'Keterangan', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('keterangan', null, ['class'=>'form-control'])}}
        {!! $errors->first('keterangan', '<p class="help-block">:message<p>') !!}
    </div>
</div>
