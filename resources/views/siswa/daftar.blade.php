@extends('layouts.app')

@section('content')
{!! Form::open(['url'=> route('siswa.pendaftaran.simpan')]); !!}
    @if(!isset($step_register))
        @include('siswa._siswa')
    @elseif($step_register == 1)
        @include('siswa._alamat')
    @elseif($step_register == 2)
        <p>form  ayah</p>
    @elseif($step_register == 3)
        <p>lokasi & syarat</p>
    @endif
{!! Form::close(); !!}
@endsection


@section('scripts')
    <script src="{{ asset('js/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.numeric.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.regex.extensions.js') }}"></script>
@endsection