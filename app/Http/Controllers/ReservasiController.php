<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use Excel;
use App\Exports\ReservasiExport;
use App\Reservasi;

class ReservasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagetitle = 'Reservasi';
        //$reservasi = Reservasi::orderBy('id_reservasi')->paginate(10);
        $reservasi = Reservasi::orderBy('tanggal','DESC')->paginate(10);

        return view('reservasi.index')->with(compact('pagetitle', 'reservasi'));
    }

    public function filter(Request $request)
    {
        $this->validate($request, [
            'start' => 'required',
            'end' => 'required'
        ]);

        $start = Carbon::parse($request->get('start'))->format('Y-m-d');
        $end = Carbon::parse($request->get('end'))->format('Y-m-d');

        $pagetitle = 'Reservasi';
        //$reservasi = Reservasi::orderBy('id_reservasi')->paginate(10);
        //$reservasi = Reservasi::whereBetween('tanggal', [$start, $end])->paginate(10);
        $reservasi = Reservasi::whereBetween('tanggal', [$start, $end]);

        $reservasi = $reservasi->paginate(10);


        return view('reservasi.index')->with(compact('pagetitle', 'reservasi', 'start', 'end'));
    }

    /**
     * Reservasi yang dilakukan oleh User Atau Admin
     */
    public function book()
    {
        //menu login di sembunyikan
        $auth_hide = 'true';
        //menu pendaftarakn di aktifkan
        $active_menu = 'pemesanan';

        //mengambil session
        $step_book = session()->get('step_book');

        //data disiapkan untuk disimpan checkout
        $data_prepared = [];

        //jika data sudah siap
        if($step_book == 2) {
            $data_prepared = $data_prepared + session()->get('data_reservasi.step_1')[0];
            $data_prepared = $data_prepared + session()->get('data_reservasi.step_2')[0];
        }

        //menampilkan view berdasarkan session yang tersimpan
        return view('reservasi.book')->with(compact('auth_hide', 'active_menu', 'step_book', 'data_prepared'));
    }

    public function bookpage($page)
    {
        //menu login di sembunyikan
        $auth_hide = 'true';
        //menu pendaftarakn di aktifkan
        $active_menu = 'pemesanan';

        //mengambil session
        $step_reservasi = session()->get('step_reservasi.'.$page);


        //menampilkan view berdasarkan session yang tersimpan
        return view('reservasi.book')->with(compact('auth_hide', 'active_menu', 'step_register'));
    }

    public function booksimpan(Request $request) {
        //check session terlebih dahulu
        $step_book = session()->get('step_book');

        //init param_validate
        $param_validate = [];

        if($step_book == 1) {
            $param_validate =[
                //'id_paket' => 'required',
                'id_jenis_ruangan' => 'required',
                'tanggal' => 'required',
                'jam_rsv' => 'required',
                'lama_sewa' => 'required',
            ];
        } else if($step_book == 2) {

        } else {
            $param_validate = [
                'nama' => 'required',
                'jenis_kelamin' => 'required',
                'nomor_telepon' => 'required|numeric',
                'email' => 'required',
            ];
        }

        //lakukan validasi        
        $this->validate($request, $param_validate);

        //init data
        $data_identitas = [];
        $data_reservasi = [];

        //assign data
        if($step_book ==1) {
            $data_reservasi =[
                'id_paket' => $request->get('id_paket'),
                'id_jenis_ruangan' => $request->get('id_jenis_ruangan'),
                'tanggal' => $request->get('tanggal'),
                'jam_rsv' => $request->get('jam_rsv'),
                'lama_sewa' => $request->get('lama_sewa'),
                'pesan' => $request->get('pesan')
            ];

        } else if($step_book == 2) {

        } else {
            $data_identitas =[
                'nama' => $request->get('nama'),
                'jenis_kelamin' => $request->get('jenis_kelamin'),
                'nomor_telepon' => $request->get('nomor_telepon'),
                'email' => $request->get('email'),
            ];
        }

        //simpan di session
        if($step_book == 1) {
            $request->session()->push('data_reservasi.step_2', $data_reservasi);
            $request->session()->put('step_book', 2);
            return redirect()->back();
        } else if( $step_book == 2) {
            $data_all = session()->get('data_reservasi.step_1')[0];
            $data_all = $data_all + session()->get('data_reservasi.step_2')[0];


            $reservasi = new Reservasi();
            $reservasi->nama = $data_all['nama'];
            $reservasi->jenis_kelamin = $data_all['jenis_kelamin'];
            $reservasi->nomor_telepon = $data_all['nomor_telepon'];
            $reservasi->email = $data_all['email'];
            $reservasi->id_paket = $data_all['id_paket'];
            $reservasi->id_jenis_ruangan = $data_all['id_jenis_ruangan'];
            $reservasi->tanggal = $data_all['tanggal'];
            $reservasi->jam_rsv = $data_all['jam_rsv'];
            $reservasi->lama_sewa = $data_all['lama_sewa'];
            $reservasi->pesan = $data_all['pesan'];
            
            /* validasi */

            $notifikasi = [];

            if($reservasi->checkStok()) {
         
                $reservasi->save();
         
                $notifikasi = [
                    'level' => 'success',
                    //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
                    'message' => 'Reservasi Berhasil',
                ];
            
            } else {
                $notifikasi = [
                    'level' => 'warning',
                    //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
                    'message' => 'Reservasi untuk tanggal ' .$data_all['tanggal'].' Sudah Penuh',
                ];
            
                Session::flash('flash_notification', $notifikasi);
            
                return redirect()->back();
            }
        
        
            Session::flash('flash_notification', $notifikasi);
            /* end validasi */

            //$reservasi = Reservasi::create($data_all);

            if($reservasi) {
                $request->session()->forget('date_reservasi');
                $request->session()->forget('step_book');
                return view('welcome');
            }


            //$request->session()->forget('date_reservasi.step_1');
            //$request->session()->forget('date_reservasi.step_2');
        } else {
            $request->session()->push('data_reservasi.step_1', $data_identitas);
            $request->session()->put('step_book', 1);
            return redirect()->back();
        }
    }

    public function bookcancel(){
        session()->forget('data_reservasi');
        session()->forget('step_book');
        return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pagetitle = 'Reservasi';

        return view('reservasi.new-reservasi')->with(compact('pagetitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'nomor_telepon' => 'required|numeric',
            'email' => 'required',
            //'id_paket' => 'required',
            'id_jenis_ruangan' => 'required',
            'tanggal' => 'required',
            'jam_rsv' => 'required',
            'lama_sewa' => 'required',
        ]);

        $reservasi = new Reservasi();
        $reservasi->nama = $request->get('nama');
        $reservasi->jenis_kelamin = $request->get('jenis_kelamin');
        $reservasi->nomor_telepon = $request->get('nomor_telepon');
        $reservasi->email = $request->get('email');
        $reservasi->id_paket = $request->get('id_paket');
        $reservasi->id_jenis_ruangan = $request->get('id_jenis_ruangan');
        $reservasi->tanggal = $request->get('tanggal');
        $reservasi->jam_rsv = $request->get('jam_rsv');
        $reservasi->lama_sewa = $request->get('lama_sewa');
        $reservasi->pesan = $request->get('pesan');


        $notifikasi = [];


        if($reservasi->checkStok()) {

            $reservasi->save();

            $notifikasi = [
                'level' => 'success',
                //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
                'message' => 'Reservasi Berhasil disimpan',
            ];

        } else {
            $notifikasi = [
                'level' => 'warning',
                //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
                'message' => 'Reservasi untuk tanggal ' .$request->get('tanggal').' Sudah Penuh',
            ];

            Session::flash('flash_notification', $notifikasi);

            return view('reservasi.renew-reservasi')->with(compact('reservasi'));
        }


        Session::flash('flash_notification', $notifikasi);

        return redirect()->route('reservasi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reservasi = Reservasi::findOrFail($id);

        return view('reservasi.edit-reservasi')->with(compact('reservasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id_jenis_ruangan' => 'required',
            'tanggal' => 'required',
            'jam_rsv' => 'required',
            'lama_sewa' => 'required',
        ]);

        $reservasi = Reservasi::findOrFail($id);
        $reservasi->id_jenis_ruangan = $request->get('id_jenis_ruangan');
        $reservasi->tanggal = $request->get('tanggal');
        $reservasi->jam_rsv = $request->get('jam_rsv');
        $reservasi->lama_sewa = $request->get('lama_sewa');


        $notifikasi = [];


        if($reservasi->checkStok()) {

            $reservasi->update();

            $notifikasi = [
                'level' => 'success',
                //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
                'message' => 'Reservasi Berhasil disimpan',
            ];

        } else {
            $notifikasi = [
                'level' => 'warning',
                //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
                'message' => 'Reservasi untuk tanggal ' .$request->get('tanggal').' Sudah Penuh',
            ];

            Session::flash('flash_notification', $notifikasi);

            return view('reservasi.renew-reservasi')->with(compact('reservasi'));
        }


        Session::flash('flash_notification', $notifikasi);

        return redirect()->route('reservasi.index');
    }

    public function arrived($id) {
        $reservasi = Reservasi::findOrFail($id);
        $reservasi->flag_status = 1;
        $reservasi->update();

            $notifikasi = [
                'level' => 'success',
                //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
                'message' => 'Reservasi Kedatangan Berhasil disimpan',
            ];

        Session::flash('flash_notification', $notifikasi);

        return redirect()->route('reservasi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservasi = Reservasi::findOrFail($id);

        $reservasi->delete();

        Session::flash('flash_notification', [
            'level' => 'warning',
            //'message' => $jenisruangan->nama_jenis_ruangan. 'Berhasil disimpan',
            'message' => ' Berhasil dihapus',
        ]);


        return redirect()->route('reservasi.index');
    }

    public function export(Request $request) {
        $this->validate($request , [
            'start' => 'required',
            'end' => 'required'
        ]);

        //return Excel::download(new ReservasiExport($request->get('start'), $request->get('end')), 'Data Reservasi.xlsx');
        //return Excel::download(new ReservasiExport($request->get('start'), $request->get('end')), 'Data Reservasi.xlsx', null, [\Maatwebsite\Excel\Excel::XLSX]);


        //$myFile = Excel::raw(new YOUR_Export_Class, \Maatwebsite\Excel\Excel::XLSX);
        $myFile = Excel::raw(
            new ReservasiExport($request->get('start'), $request->get('end')),
            \Maatwebsite\Excel\Excel::XLSX
        );
        
        $response =  array(
           'name' => "Laporan Reservasi ".$request->get('start')." s/d ".$request->get('end').".xlsx", //no extention needed
           'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );
        
        return response()->json($response);
    }
}
