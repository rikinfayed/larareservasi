@extends('layouts.app')

@section('content')

<section class="herobwa mt-5">
    <div class="container">
        <div class="row">
            <div class="col align-self-center">
                <h1 class="mb-4">HERO TEXT</h1>
                <p class="mb-4">Deskripsi Text</p>
                <a href="{{ route('book') }}" class="btn btn-hero" role="button">Book Now</a>
            </div>
            <div class="col d-none d-sm-none d-md-block">
                <img width="300" src="{{ asset('/images/happy-puppy-logo.png') }}" alt="">
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container mt-5">
        @if(isset($jnsruangans) && $jnsruangans->count() > 0)
        <div class="col">
            <h2 class="text-center">Our Rooms</h2>
        </div>
        <div class="row mt-5">
            @foreach( $jnsruangans as $jnsruangan)
            <div class="col-md-4 mb-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="{{ asset('img/'.$jnsruangan->foto) }}" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title">{{ $jnsruangan->nama_jenis_ruangan }}</h5>
                    <p class="card-text">Per Jam Rp. {{ $jnsruangan->harga }}-</p>
                    <p class="card-text">{{ $jnsruangan->keterangan }}</p>
                    <!-- a href="#" class="btn btn-primary">Go somewhere</a-->
                  </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif

        @if(isset($pktruangans) && $pktruangans->count() > 0)
        <div class="col mt-5">
            <h2 class="text-center">Paket / Promo</h2>
        </div>
        <div class="row mt-5">
            @foreach( $pktruangans as $pktruangan)
            <div class="col-md-4 mb-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="{{ asset('img/'.$pktruangan->foto_paket) }}" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title">{{ $pktruangan->nama_paket }}</h5>
                    <p class="card-text">Harga Paket Rp. {{ $pktruangan->harga_paket }}</p>
                  </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</section>
@endsection
