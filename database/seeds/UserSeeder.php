<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

              //buat role admin 
              $roleAdmin = new Role(); 
              $roleAdmin->name='admin'; 
              $roleAdmin->display_name = "Admin"; 
              $roleAdmin->save(); 
               
              //buat role staf 
              $roleStaf = new Role(); 
              $roleStaf->name = "staf"; 
              $roleStaf->display_name = "Staf"; 
              $roleStaf->save(); 
               
                
              //buat user admin 
              $admin = new User(); 
              $admin->name = 'Rudianto'; 
              $admin->username = 'rudi01'; 
              $admin->email = 'rudianto@gmail.com'; 
              $admin->password = bcrypt('password'); 
              $admin->save(); 
              $admin->attachRole($roleAdmin); 
                 
              //buat user staf 
              $admin = new User(); 
              $admin->name = 'Tika'; 
              $admin->username = 'tika02'; 
              $admin->email = 'Tika@gmail.com'; 
              $admin->password = bcrypt('password'); 
              $admin->save(); 
              $admin->attachRole($roleStaf); 
      
    }
}
