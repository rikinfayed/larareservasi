<div class="row">
    <div class="form-group row col-md-6">
        {{ Form::label('', 'Gunakan Template Import Excel', ['class'=>'col-form-label col-md-4']) }}
        <div class="col-md-8">
            <a href="{{ route('kelurahan.generate.template') }}" class="btn btn-info">Download</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group row col-md-6">
        {{ Form::label('excel', 'Pilih File', ['class'=>'col-form-label col-md-4']) }}
        <div class="col-md-8">
        {{ Form::file('excel') }}
        {{ $errors->has('excel') ? $errors->first('excel'): ''}}
        </div>
    </div>
</div>