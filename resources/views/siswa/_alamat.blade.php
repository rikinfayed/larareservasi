<section class="formstepreg mt-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-transparent pb-0">
                        <h3 class="text-center mb-3">Form Pendaftaran Siswa Baru</h3>
                        <div class="row">
                            <div class="col text-center steped pb-2">
                                <p class="mb-0">Data Calon Siswa</p>
                            </div>
                            <div class="col text-center steped pb-2">
                                <p class="mb-0">Alamat & Lokasi</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Ayah Kandung</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Ibu Kandung</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Upload File Persyaratan</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body mt-4">
                        <div class="row">
                            <div class="col-4">
                                <h4>Informasi</h4>
                                <p>Untuk menajadi perhatianUntuk menajadi perhatianUntuk menajadi perhatian</p>
                            </div>
                            <div class="col-8">
                                <form>
                                    <div class="form-group">
                                        {!! Form::label('koordinat_tmpt_tnggl', 'Koordinat Tempat Tinggal', ['class' => '']) !!}
                                        <div class="input_group row">
                                            <div class="col-md-6">
                                                {!! Form::text('koordinat_tmpt_tnggl', null, ['class'=> $errors->has('almt_tmpt_tnggl') ? 'form-control is-invalid': 'form-control', 'autofocus']) !!}
                                                {!! $errors->first('koordinat_tmpt_tnggl', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                            </div>
                                            <div class="col-md-4">
                                                Icon Map
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('radius_tmpt_tnggl', 'Jarak Radius Tempat Tinggal', ['class'=> '']) !!}
                                        {!! Form::text('radius_tmpt_tnggl', null, ['class'=>$errors->has('radius_tmpt_tnggl') ? 'form-control is-invalid': 'form-control', 'disabled']) !!}
                                        {!! $errors->first('radius_tmpt_tnggl', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('almt_tmpt_tnggl', 'Alamat Tempat Tinggal', ['class'=> '']) !!}
                                        {!! Form::textarea('almt_tmpt_tnggl', null, ['class'=>$errors->has('almt_tmpt_tnggl') ? 'form-control is-invalid': 'form-control']) !!}
                                        {!! $errors->first('almt_tmpt_tnggl', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('no_rt', 'No. RT', ['class'=> '']) !!}
                                        {!! Form::text('no_rt', null, ['class'=>$errors->has('no_rt')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('no_rt', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                     <div class="form-group">
                                        {!! Form::label('no_rw', 'No. Rw', ['class'=> '']) !!}
                                        {!! Form::text('no_rw', null, ['class'=>$errors->has('no_rw')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('no_rw', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('kelurahan', 'Nama Kelurahan', ['class'=> '']) !!}
                                        {!! Form::text('kelurahan', null, ['class'=>$errors->has('kelurahan')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('kelurahan', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('kecamatan', 'Nama Kecamatan', ['class'=> '']) !!}
                                        {!! Form::text('kecamatan', null, ['class'=>$errors->has('kecamatan')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('kecamatan', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('kode_pos', 'Kode Pos', ['class'=> '']) !!}
                                        {!! Form::text('kode_pos', null, ['class'=>$errors->has('kode_pos')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('kode_pos', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('jns_transportasi_id', 'Alat Transportasi ke Sekolah', ['class'=> '']) !!}
                                        {!! Form::select('jns_transportasi_id',['' => '== Pilih ==']+App\Transportasi::pluck('nama', 'id')->all(), null, ['class'=>$errors->has('jns_transportasi_id')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('jns_transportasi_id', '<span class="invalid-feedback" alert="role">:message</span>') !!}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-center">
                                <!--li class="page-item disabled">
                                    <span class="page-link">Sebelumnya</span>
                                </li-->
                                <li class="page-item">
                                    <a href="{{route('siswa.pendaftaran.page', 1)}}" class="page-link">Sebelumnya</a>
                                </li>
                                <li class="page-item">
                                    {!! Form::submit('Simpan & Lanjutkan', ['class'=> 'page-link btn-primary']) !!}
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>