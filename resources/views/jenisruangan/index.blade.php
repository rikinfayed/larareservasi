@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header d-flex p-0">
                    <h3 class="card-title p-3">Daftar Jenis Ruangan</h3>
                    <ul class="nav nav-pills ml-auto p-2">
                        <li class="nav-item"><a class="nav-link" href="{{route('jenisruangan.create')}}"><i class="fa fa-plus"></i> Tambah Jenis Ruangan</a></li>
                        <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" >
                                    <i class="fa fa-bars"></i>
                                </a>
                                <div class="dropdown-menu">
                                    <!--a class="dropdown-item" tabindex="-1" href="#" style="color:black">
                                        <i class="fa fa-refresh fa-spin"></i>
                                        Action
                                        <span class="float-right text-muted text-sm"></span>
                                    </a-->
                                    <div class="divider"></div>
                                    <!--a class="dropdown-item" tabindex="-1" href="route('kelurahan.import.excel')"-->
                                    <a class="dropdown-item" tabindex="-1" href="#">
                                        <i class="fa fa-upload" style="color:black;"></i>
                                        <span style="color:black">Impor dari Excel</span>
                                        <span class="float-right text-muted text-sm"></span>
                                    </a>
                                    <!--a class="dropdown-item" tabindex="-1" href="route('kelurahan.export.excel')"-->
                                    <a class="dropdown-item" tabindex="-1" href="#">
                                        <i class="fa fa-file" style="color:black;"></i>
                                        <span style="color:black">Ekspor Data Kelurahan ke Excel</span>
                                        <span class="float-right text-muted text-sm"></span>
                                    </a>
                                </div>
                        </li> 
                    </ul>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Ruangan</th>
                                    <th>Harga</th>
                                    <th>Foto</th>
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($jenisruangan) && $jenisruangan->count() > 0)
                                <?php $i = $jenisruangan->FirstItem(); $datesss = 'test'; ?>
                                @foreach($jenisruangan as $jnsruang)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $jnsruang->nama_jenis_ruangan }}</td>
                                        <td>{{ $jnsruang->harga }}</td>
                                        <td>{{ $jnsruang->foto }}</td>
                                        <td>{{ $jnsruang->keterangan }}</td>
                                        <td>
                                            <a href="{{ route('jenisruangan.edit', $jnsruang->id_jenis_ruangan) }}"class="btn-info btn-sm">Edit</a>
                                            <a href="#" class="btn-danger btn-sm" onclick=" var check = confirm('Yakin Hapus {{ $jnsruang->nama_jenis_ruangan }}?'); if(check) {event.preventDefault();document.getElementById('formdelete{{ $jnsruang->id_jenis_ruangan }}').submit();}" >Hapus</a>
                                            {{ Form::open(['url'=>route('jenisruangan.destroy', $jnsruang->id_jenis_ruangan), 'method'=>'delete', 'id'=>'formdelete'.$jnsruang->id_jenis_ruangan, 'class'=>'form-inline', 'data-confirm'=>'Yakin Hapus '.$jnsruang->nama_jenis_ruangan])}}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                @else
                                    <tr><td colspan="6" class="text-center"> Data Kosong </td></tr>
                                @endif
                            </body>
                        </table>
                    </div>
                    <div class="float-left pagination-md mb-0 clear-fix">
                          @if(isset($jenisruangan) && $jenisruangan->count() > 0)
                            Menampilkan {{ $jenisruangan->FirstItem() }} sampai {{$jenisruangan->LastItem()}} Dari {{$jenisruangan->total()}} Entri
                          @endif
                    </div>
                    <div class="float-right pagination-md mb-0 clear-fix">
                          @if(isset($jenisruangan) && $jenisruangan->count() > 0)
                            {{$jenisruangan->links()}}
                          @endif
                    </div>
                </div>
                <!--div class="overlay preload">
                    <i class="fa fa-refresh fa-spin"></i>
                </div-->
            </div>
        </div>

        <!--div class="card col-md-4">
            <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg">$18,230.00</span>
                    <span>Sales Over Time</span>
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-success">
                      <i class="fa fa-arrow-up"></i> 33.1%
                    </span>
                    <span class="text-muted">Since last month</span>
                  </p>
                </div>


                <div class="position-relative mb-4">
                  <canvas id="sales-chart" height="200"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fa fa-square text-primary"></i> This year
                  </span>

                  <span>
                    <i class="fa fa-square text-gray"></i> Last year
                  </span>
                </div>
            </div>
        </div-->

    </div>
        <!-- iframe class="container-fluid" height="800px" src="{{url('print/pdf')}}"></iframe-->
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.preload').fadeOut();
        });
    </script>
@endsection