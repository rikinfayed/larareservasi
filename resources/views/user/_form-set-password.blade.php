<div class="form-group row{{ $errors->has('password') ?' has-error': ''}}">
    {!! Form::label('password', 'Password', ['class'=>'text-md-right col-label col-md-4']) !!}
    <div class="col-md-4">
        {!! Form::password('password', ['class'=>'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('password-confirm', 'Re-Password', ['class'=>'text-md-right col-label col-md-4']) !!}
    <div class="col-md-4">
        {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
    </div>
</div>
