<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Jenisruangan;

class Ruangan extends Model
{
   protected $table = 'ruangan';

    public $primaryKey = 'id_ruangan';

    protected $fillable = [
        'nama_ruangan',
        'nomor',
        'status',
        'id_jenis_ruangan'
    ];

    public function jenisruangan() {
        return Jenisruangan::where('id_jenis_ruangan',$this->id_jenis_ruangan);
    }
}
