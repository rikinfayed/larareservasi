<div class="form-group row{{ $errors->has('name') ? ' has-error': ''}}">
    {!! Form::label('name', 'Nama', ['class'=>'text-md-right col-form-label col-md-4'])!!}
    <div class="col-md-4">
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row{{ $errors->has('username')? ' has-error': ''}}">
    {!! Form::label('username', 'User Name', ['class'=>'text-md-right col-label col-md-4']) !!}
    <div class="col-md-4">
        {!! Form::text('username', null, ['class'=>'form-control']) !!}
        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row{{ $errors->has('email')? ' has-error': ''}}">
    {!! Form::label('email', 'Alamat Email', ['class'=>'text-md-right col-label col-md-4']) !!}
    <div class="col-md-4">
        {!! Form::text('email', null, ['class'=>'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row{{ $errors->has('password') ?' has-error': ''}}">
    {!! Form::label('password', 'Password', ['class'=>'text-md-right col-label col-md-4']) !!}
    <div class="col-md-4">
        {!! Form::password('password', ['class'=>'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('password-confirm', 'Re-Password', ['class'=>'text-md-right col-label col-md-4']) !!}
    <div class="col-md-4">
        {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
    </div>
</div>
<div class="form-group row{{ $errors->has('role')? ' has-error':''}}">
    {!! Form::label('role', 'Hak Akses', ['class'=>'text-md-right col-label col-md-4']) !!}
    <?php if(isset($role)) {
        $selected = $role;
    } else {
        $selected = 'null';
    }
    ?>
    <div class="col-md-4">
        {{ Form::select('role', [''=>'== Pilih ==']+$roles, $selected , ['class'=>'form-control']) }}
        {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
    </div>
</div>