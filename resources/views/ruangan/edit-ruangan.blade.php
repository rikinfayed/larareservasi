@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
               <div class="card card-info">
                   <div class="card-header">
                        <h3 class="card-title">Edit Ruangan {{ $ruangan->nama}}</h3>
                   </div>
                   <div class="card-body">
                        {!! Form::model($ruangan,['url'=> route('ruangan.update', $ruangan->id_ruangan),'class'=>'form-horizontal','method'=>'put', 'files'=>'true'])!!}
                            @include('ruangan._form-ruangan')
                            
                            <div class="col-md-4 col-md-offset-4">
                                {{ Form::submit('Simpan', ['class'=>'btn btn-primary'])}}
                                <a href="{{ route('ruangan.index') }}" class="btn btn-info">Kembali</a>
                            </div>
                        {!! Form::close()!!}
                   </div>
               </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="http://localhost:8000/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-mask]').inputmask('9999');
        });
    </script>
@endsection
