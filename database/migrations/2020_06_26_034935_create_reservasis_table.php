<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasi_ruangan', function (Blueprint $table) {
            $table->id('id_reservasi');
            //step 1
            $table->string('nama');
            $table->enum('jenis_kelamin', ['laki-laki', 'perempuan']);
            $table->string('nomor_telepon');
            $table->string('email');
            //step 2
            $table->bigInteger('id_paket')->unsigned();
            $table->bigInteger('id_jenis_ruangan')->unsigned();
            $table->date('tanggal');
            $table->time('jam_rsv');
            $table->integer('lama_sewa');
            $table->string('pesan')->nullable();
            $table->string('flag_status')->nullable();

            $table->timestamps();

            $table->foreign('id_paket')->references('id_paket')->on('paket_ruangan');
            $table->foreign('id_jenis_ruangan')->references('id_jenis_ruangan')->on('jenis_ruangan');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasi_ruangan');
    }
}
