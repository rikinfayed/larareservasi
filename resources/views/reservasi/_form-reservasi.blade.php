<div class="form-group {{ $errors->has('nama') ? ' has-error' : ''}}">
    {{ Form::label('nam', 'Nama', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('nama', null, ['class'=>'form-control']) }}
        {!! $errors->first('nama', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('jenis_kelamin') ? ' has-error' : ''}}">
    {{ Form::label('jenis_kelamin', 'Jenis Kelamin', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::select('jenis_kelamin',['' => '== Pilihan ==', 'perempuan' => 'Perempuan', 'laki-laki' => 'Laki-Laki'], null, ['class'=>'form-control']) }}
        {!! $errors->first('jenis_kelamin', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('tanggal') ? ' has-error' : ''}}">
    {{ Form::label('tanggal', 'Tanggal Reservasi', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4 input-group">
        {{ Form::date('tanggal', null, ['class'=>'form-control']) }}
        <span class="input-group-prepend">
        <span class="input-group-text">
            <i class="fa fa-calendar"></i>
        </span>
        </span>
        {!! $errors->first('tanggal', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('jam_rsv') ? ' has-error' : ''}}">
    {{ Form::label('jam_rsv', 'Jam Reservasi', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4 input-group">
        {{ Form::time('jam_rsv', null, ['class'=>'form-control']) }}
        <span class="input-group-prepend">
        <span class="input-group-text">
            <i class="fa fa-clock-o"></i>
        </span>
        </span>
        {!! $errors->first('jam_rsv', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<?php
    $lama_sewa = [
        '1' => '1 Jam',
        '2' => '2 Jam',
        '3' => '3 Jam',
        '4' => '4 Jam',
        '5' => '5 Jam',
        '6' => '6 Jam',
    ];
?>
<div class="form-group {{ $errors->has('lama_sewa') ? ' has-error' : ''}}">
    {{ Form::label('lama_sewa', 'Lama Sewa', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::select('lama_sewa', [''=>'Pilihan']+$lama_sewa,null, ['class'=>'form-control']) }}
        {!! $errors->first('lama_sewa', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('nomor_telepon') ? ' has-error' : ''}}">
    {{ Form::label('nomor_telepon', 'Nomor HP', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('nomor_telepon', null, ['class'=>'form-control']) }}
        {!! $errors->first('nomor_telepon', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
    {{ Form::label('email', 'Alamat Email', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::text('email', null, ['class'=>'form-control']) }}
        {!! $errors->first('email', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('id_jenis_ruangan') ? ' has-error' : ''}}">
    {{ Form::label('id_jenis_ruangan', 'Jenis Ruangan', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::select('id_jenis_ruangan',['' => '== Pilihan ==']+App\Jenisruangan::pluck('nama_jenis_ruangan', 'id_jenis_ruangan')->all(), null, ['class'=>'form-control']) }}
        {!! $errors->first('id_jenis_ruangan', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('id_paket') ? ' has-error' : ''}}">
    {{ Form::label('id_paket', 'Paket/Promo', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::select('id_paket',['' => '== Pilihan ==']+App\Paketruangan::pluck('nama_paket', 'id_paket')->all(), null, ['class'=>'form-control']) }}
        {!! $errors->first('id_paket', '<p class="help-block">:message<p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('pesan') ? ' has-error' : ''}}">
    {{ Form::label('pesan', 'Note', ['class'=>'control-label col-md-4']) }}
    <div class="col-md-4">
        {{ Form::textarea('pesan', null, ['class'=>'form-control']) }}
        {!! $errors->first('pesan', '<p class="help-block">:message<p>') !!}
    </div>
</div>
