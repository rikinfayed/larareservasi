@extends('layouts.app')

@section('content')
{!! Form::model($data_siswa, ['url'=> route('siswa.pendaftaran.simpan')]); !!}
    @if($page == 1)
        @include('siswa._siswa')
    @elseif($page == 2)
        @include('siswa._alamat')
    @elseif($page == 3)
        <p>lokasi & syarat</p>
    @endif
{!! Form::close(); !!}
@endsection


@section('scripts')
    <script src="{{ asset('js/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.numeric.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.regex.extensions.js') }}"></script>
@endsection