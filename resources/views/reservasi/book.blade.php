@extends('layouts.app')

@section('content')
@if($step_book == 2)
    {!! Form::model($data_prepared, ['url'=> route('book.simpan')]); !!}
@else
    {!! Form::open(['url'=> route('book.simpan')]); !!}
@endif
        @if(!isset($step_book))
            @include('reservasi._identitas')
        @elseif($step_book == 1)
            @include('reservasi._order')
        @elseif($step_book == 2)
            @include('reservasi._checkout')
        @endif
    {!! Form::close(); !!}
@endsection


@section('scripts')
    <script src="{{ asset('js/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.numeric.extensions.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.regex.extensions.js') }}"></script>
@endsection