@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <p>Form New User</p>
                            {!! Form::open(['url'=> route('user.store')]) !!}
                                @include('user._form-user')
                                <div class="form-group row">
                                    <div class="col-md-4 offset-md-4">
                                        {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                        <a href="{{ route('user.index') }}" class="btn btn-info">Kembali</a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection