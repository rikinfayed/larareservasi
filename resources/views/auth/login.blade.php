@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    {!! Form::open(['url'=> 'login']) !!}

                        <div class="form-group row">
                            {!! Form::label('username', 'Username', ['class'=>'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                {!! Form::text('username', null, ['class'=> $errors->has('username')?'form-control is-invalid': 'form-control', 'autofocus', 'required']) !!}

                                {!! $errors->first('username', '<span class="invalid-feedback" role="alert"><strong>:message</strong></span>') !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('password','Password',['class'=>'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                {!! Form::password('password',['class'=> 'form-control', 'required', 'autocomplete'=>'current-password']) !!}
                                {!! $errors->first('password', '<span class="invalid-feedback" role="alert"><strong>:message</strong></span>') !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Ingat Saya') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                {!! Form::submit('Masuk', ['class'=>'btn btn-primary']) !!}

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Lupa Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
