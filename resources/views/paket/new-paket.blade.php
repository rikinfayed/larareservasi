@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
               <div class="card card-info">
                   <div class="card-header">
                        <h3 class="card-title">Form New Paket</h3>
                   </div>
                   <div class="card-body">
                        {!! Form::open(['url'=> route('paket.store'),'method'=>'post', 'files'=>'true','class'=>'form-horizontal'])!!}
                            @include('paket._form-paket')
                            <div class="col-md-4">
                                {{ Form::submit('Simpan', ['class'=>'btn btn-primary'])}}
                                <a href="{{ route('jenisruangan.index') }}" class="btn btn-info">Kembali</a>
                            </div>
                        {!! Form::close()!!}
                   </div>
               </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="http://localhost:8000/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-mask]').inputmask('9999');
        });
    </script>
@endsection
