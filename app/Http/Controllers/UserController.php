<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Session;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        //$user = User::where('id','!=', $user->id)->paginate(5); //tidak menampilkan Admin yang aktif
        $user = User::orderBy('id')->paginate(5); //menampilkan semua user
        //for view
        $pagetitle = 'Manage Accounts';
        return view('user.index')->with(compact('user','pagetitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','id')->all();
        //return $role;
        //for view
        $pagetitle = 'Manage Accounts';
        return view('user.new-user')->with(compact('roles', 'pagetitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'username' => 'required|string|unique:users',
            'email' => 'required|string|unique:users', //tambah email untuk reset password
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required'
        ]);

        $user = new User();
        $user->name = $request->get('name');
        $user->username = $request->get('username');
        $user->email = $request->get("email");
        $user->password = bcrypt($request->get('password'));
        $user->save();

        $user->roles()->sync([$request->get('role')]);

        Session::flash('flash_notification', [
            'level' => 'success',
            'message' => $user->name .' berhasil disimpan',
        ]);

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //for data
        $roles = Role::pluck('name', 'id')->all();
        $user = User::find($id);
        $role = $user->roles->first()->id;
        
        return view('user.show-user')->with(compact('user', 'roles','role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //for data
        $roles = Role::pluck('name','id')->all();
        $user = User::find($id);
        $role = $user->roles->first()->id;

        $pagetitle = 'Manage Accounts';
        return view('user.edit-user')->with(compact('user','roles', 'role', 'pagetitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'name' => 'required|string|unique:users,name,'.$id,
           'email' => 'required|string|unique:users,email,'.$id,
           'role' => 'required|exists:roles,id'
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->save();

        $user->roles()->sync([$request->get('role')]);
        
        Session::flash('flash_notification', [
            'level' => 'success',
            'message' => $user->name.' Berhasil diperbarui',
        ]);

        //return redirect()->route('user.index');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usr = User::findOrFail($id);

        //hapus akun
        $usr->delete();

        //notifikasi
        Session::flash('flash_notification', [
            'level' => 'info',
            'message' => $usr->name.' Berhasil dihapus',
        ]);

        //kembali ke halaman
        return redirect()->route('user.index');
    }

    public function setpassword($id)
    {
        //for data
        $roles = Role::pluck('name','id')->all();
        $user = User::find($id);
        $role = $user->roles->first()->id;

        return view('user.set-password')->with(compact('user','roles', 'role'));
    }

    public function updatepassword(Request $request, $id) {
        $this->validate($request, [
            'password' => 'required|string|min:5|confirmed',
        ]);

        $user = User::findOrFail($id);
        $user->password = bcrypt($request->get('password'));
        $user->save();

        Session::flash('flash_notification', [
            'level' => 'success',
            'message' => 'Password berhasil diganti',
        ]);

        return redirect()->route('user.index');
    }
}
