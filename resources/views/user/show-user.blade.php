@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('user.index') }}">Daftar Akun</a> > Informasi User 
                    </div>
                    <div class="card-body">
                        <p>Informasi User</p>
                            {!! Form::model($user, ['url'=> route('user.update', $user->id),'method'=>'put']) !!}
                                @include('user._form-user')
                                <div class="form-group">
                                    <div class="col-md-4 offset-md-4">
                                        {!! Form::submit('Simpan', ['type'=>'button','class'=>'btn btn-primary']) !!}
                                    </div>
                                </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
