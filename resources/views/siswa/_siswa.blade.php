<section class="formstepreg mt-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-transparent pb-0">
                        <h3 class="text-center mb-3">Form Pendaftaran Siswa Baru</h3>
                        <div class="row">
                            <div class="col text-center steped pb-2">
                                <p class="mb-0">Data Calon Siswa</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Alamat & Lokasi</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Ayah Kandung</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Ibu Kandung</p>
                            </div>
                            <div class="col text-center pb-2">
                                <p class="mb-0">Upload File Persyaratan</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body mt-4">
                        <div class="row">
                            <div class="col-4">
                                <h4>Informasi</h4>
                                <ul>
                                    <li><p>Pastikan data terinput dengan benar</p></li>
                                </ul>
                            </div>
                            <div class="col-8">
                                <form>
                                    <div class="form-group">
                                        {!! Form::label('nama_lgkp', 'Nama Lengkap', ['class' => '']) !!}
                                        {!! Form::text('nama_lgkp', null, ['class'=> $errors->has('nama_lgkp') ? 'form-control is-invalid': 'form-control', 'autofocus']) !!}
                                        {!! $errors->first('nama_lgkp', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('jns_klmin', 'Jenis Kelamin', ['class' => '']) !!}
                                        <div class="form-inline {{ $errors->has('jns_klmin') ? 'is-invalid' : ''}} ">
                                            {!! Form::radio('jns_klmin','LAKI-LAKI', null,['class'=>'form-check-input mx-3 mb-3']) !!}
                                                <p>Laki-Laki</p>
                                            {!! Form::radio('jns_klmin','PEREMPUAN', null,['class'=>'form-check-input mx-3 mb-3']) !!}
                                                <p>Perempuan</p>
                                        </div>
                                        {!! $errors->first('jns_klmin', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('tgl_lhr', 'Tanggal Lahir', ['class'=> '']) !!}
                                        {!! Form::text('tgl_lhr', null, ['class'=>$errors->has('tgl_lhr') ? 'form-control is-invalid': 'form-control']) !!}
                                        {!! $errors->first('tgl_lhr', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('tmpt_lhr', 'Tempat Lahir', ['class'=> '']) !!}
                                        {!! Form::text('tmpt_lhr', null, ['class'=>$errors->has('tmpt_lhr') ? 'form-control is-invalid': 'form-control']) !!}
                                        {!! $errors->first('tmpt_lhr', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('nik', 'NIK', ['class'=> '']) !!}
                                        {!! Form::text('nik', null, ['class'=>$errors->has('nik')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('nik', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('kk', 'NO. KK', ['class'=> '']) !!}
                                        {!! Form::text('kk', null, ['class'=>$errors->has('kk')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('kk', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('no_akta', 'NO. Akta Lahir', ['class'=> '']) !!}
                                        {!! Form::text('no_akta', null, ['class'=>$errors->has('no_akta')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('no_akta', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('agama_id', 'Agama / Kepercayaan', ['class'=> '']) !!}
                                        {!! Form::select('agama_id',['' => '== Pilih ==']+App\Agama::pluck('nama', 'id')->all(), null, ['class'=>$errors->has('agama_id')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('agama_id', '<span class="invalid-feedback" alert="role">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('kbkh', 'Berkebutuhan Khusus', ['class'=>' ']) !!}
                                        <div class="form-inline is-invalid">
                                            {!! Form::radio('kbkh', '1', null, ['class'=> 'form-check-input mx-3 mb-3']) !!}
                                                <p>Ya</p>
                                            {!! Form::radio('kbkh', '0', null, ['class'=> 'form-check-input mx-3 mb-3']) !!}
                                                <p>Tidak</p>
                                        </div>
                                        {!! $errors->first('kbkh', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('wrg_ngr', 'Kewarganegaraan') !!}
                                        {!! Form::select('wrg_ngr', ['' => '== Pilih ==', 'WNI' => 'Indonesia (WNI)', 'WNA' => 'Asing (WNA)'], null, ['class'=> $errors->has('wrg_ngr')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('wrg_ngr', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('anak_ke', 'Anak Ke', ['class'=>'']) !!}
                                        {!! Form::text('anak_ke', null, ['class'=>$errors->has('anak_ke')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('anak_ke', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('jml_sdr_kndung', 'Jumlah Saudara Kandung', ['class'=> '']) !!}
                                        {!! Form::text('jml_sdr_kndung', null, ['class'=>$errors->has('jml_sdr_kndung')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('jml_sdr_kndung', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('tgi_bdn', 'Tinggi Badan', ['class'=>null]) !!}
                                        {!! Form::text('tgi_bdn', null, ['class'=>$errors->has('tgi_bdn')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('tgi_bdn', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('brt_bdn', 'Berat Badan', ['class'=>null]) !!}
                                        {!! Form::text('brt_bdn', null, ['class'=>$errors->has('brt_bdn')? 'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('brt_bdn', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="">Asal Sekolah</label>
                                        {!! Form::label('sekolahasl_id', 'Sekolah Asal', ['class'=> '']) !!}
                                        {!! Form::select('sekolahasl_id', [''=>'== Pilih ==',]+App\Sekolahasl::pluck('nama', 'id')->all(),null, ['class'=>$errors->has('sekolahasl_id')?'form-control is-invalid':'form-control']) !!}
                                        {!! $errors->first('sekolahasl_id', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-center">
                                <li class="page-item">
                                    {!! Form::submit('Simpan & Lanjutkan', ['class'=> 'page-link btn-primary']) !!}
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>