@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <p>Informasi User</p>
                            {!! Form::model($user, ['url'=> route('user.update', $user->id), 'class'=>'form-horizontal', 'method'=>'put']) !!}
                                @include('user._form-edit-user')
                                <div class="form-group">
                                    <div class="col-md-4 offset-md-4">
                                        {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                        <a href="{{ route('user.index') }}" class="btn btn-info">Batal</a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
