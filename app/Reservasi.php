<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Kelas;
use App\Jenisruangan;
use App\Ruangan;
use App\Paketruangan;

class Reservasi extends Model
{
    protected $table = 'reservasi_ruangan';

    public $primaryKey = 'id_reservasi';

    protected $fillable = [
        'nama',
        'jenis_kelamin',
        'nomor_telepon',
        'email',
        'id_paket',
        'id_jenis_ruangan',
        'tanggal',
        'jam_rsv',
        'lama_sewa',
        'pesan'
    ];

    //protected $fillable = ['tanggal', 'kelas_id', 'slot_id', 'stok_number'];
    
    // Tanggal Lahir Mutable
    /*public function getTanggalAttribute() {
        $value = $this->attributes['tanggal'];
        return Carbon::parse($value)->format('d-m-Y');
    }*/
    
    public function setTanggalAttribute($value) {
         $this->attributes['tanggal'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function jenisruangan() {
        return Jenisruangan::find($this->id_jenis_ruangan);
    }

    public function paketruangan() {
        return Paketruangan::find($this->id_paket);
    }

    public function checkStok() {
        //tanggal
        $tanggal = Carbon::parse($this->tanggal)->format('Y-m-d');
        //jenis ruang
        $id_jenis_ruangan = $this->id_jenis_ruangan;
        //check room yang di booking
        $booked_room_by_date = $this::where([
            ['id_jenis_ruangan','=', $id_jenis_ruangan],
            ['tanggal','=', $tanggal]]
        )
        ->count();
        //jumlah room - room yang di booking
        $stok = $this->jenisruangan()->ruangan()
        ->where('status', 'open')
        ->count();

        //jumlah tersedia
        $tersedia = $stok - $booked_room_by_date;
        //status
        $status = false;

        if($tersedia > 0) {
            $status = true;
        }
        //return $booked_room_by_date;
        return $status;
    }

    public function avaibleCount() {
        $booked_room_by_date = $this::where([
            ['tanggal','=', $this->tanggal]]
        )
        ->count();

        //jumlah semua room - room
        $stok = Ruangan
        ::where('status', 'open')
        ->count();

        //jumlah tersedia
        $tersedia = $stok - $booked_room_by_date;

        return $tersedia;
    }

    public function roomCount() {
        $stok = Ruangan
        ::where('status', 'open')
        ->count();

        //jumlah tersedia
       return $stok;
    }

}
