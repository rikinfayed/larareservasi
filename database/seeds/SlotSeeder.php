<?php

use Illuminate\Database\Seeder;
use App\Slot;

class SlotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slot::create(['start_time' => '08:00', 'end_time'=>'09:00']);
        Slot::create(['start_time' => '09:00', 'end_time'=>'10:00']);
        Slot::create(['start_time' => '10:00', 'end_time'=>'11:00']);
        Slot::create(['start_time' => '11:00', 'end_time'=>'12:00']);
        Slot::create(['start_time' => '12:00', 'end_time'=>'13:00']);
        Slot::create(['start_time' => '13:00', 'end_time'=>'14:00']);
        Slot::create(['start_time' => '14:00', 'end_time'=>'15:00']);
        Slot::create(['start_time' => '15:00', 'end_time'=>'16:00']);
    }
}
