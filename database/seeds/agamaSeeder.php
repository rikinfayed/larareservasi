<?php

use Illuminate\Database\Seeder;
use App\Agama;

class agamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agama = new Agama();
        $agama->nama = 'ISLAM';
        $agama->save();

        $agama = new Agama();
        $agama->nama = 'KRISTEN';
        $agama->save();

        $agama = new Agama();
        $agama->nama = 'BUDDHA';
        $agama->save();

        $agama = new Agama();
        $agama->nama = 'KATHOLIK';
        $agama->save();

        $agama = new Agama();
        $agama->nama = 'KHONGHUCU';
        $agama->save();
    }
}
